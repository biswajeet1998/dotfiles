packagesPacman=(
    "qtile" 
    "picom"
    "scrot"
    "slock"
    "xorg"
    "xorg-xinit"
    "nitrogen"
    "python-dbus-next"
    "python-iwlib"
    "xclip"
);

packagesYay=(
    "qtile-extras"
);
